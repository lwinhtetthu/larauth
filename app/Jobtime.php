<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobtime extends Model
{
    
	public function jobs(){
		return $this->hasMany(Job::class);
	}

}
