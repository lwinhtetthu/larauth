<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Gravatar;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable=['email','password','firstname','lastname','phone','title','jobtype_id','level_id','location_id','cv',];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /*hash password*/
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function getId() {
        return $this->id;
    }

    public function getGravatarAttribute(){
        return Gravatar::fallback('http://urlto.example.com/avatar.jpg')->get($this->email, ['size' => 50]);
    }

    public function jobcv(){
        return $this->hasMany(Jobcv::class);
      }
}
