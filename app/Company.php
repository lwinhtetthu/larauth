<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
  
    protected $fillable = ['companyname', 'email','phone','aboutcompany','logo'];

    public function jobs(){
    	return $this->hasMany(Job::class);
    }

    public function jobcv(){
	    return $this->hasMany(Jobcv::class);
	  }

}
