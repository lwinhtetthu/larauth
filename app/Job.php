<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\JobResource;

class Job extends Model
{
  protected $fillable = ['jobtitle','companyname', 'salary','location','jobtime_id','jobtype','description','requirement','aboutcompany'];

  public function company(){
  	return $this->belongsTo(Company::class, 'companyname');
  }


  public function getCompany(){
  	$company = $this->company;
  	if($company instanceof Company){
  		JobResource::withoutWrapping();
  		return new JobResource($company);
  	}
  	return 'wrong';
  }

  public function jobtypes(){
    return $this->belongsTo(Jobtype::class, 'jobtype');
  }

  public function jobtime(){
    return $this->belongsTo(Jobtime::class, 'jobtime_id');
  }

  public function jobcv(){
    return $this->hasMany(Jobcv::class);
  }

}
