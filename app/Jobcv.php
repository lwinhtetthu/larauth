<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobcv extends Model
{
    protected $fillable = ['user_id', 'job_id','company_id'];

    public function users(){
	  	return $this->belongsTo(User::class, 'user_id');
	}

    public function jobs(){
	  	return $this->belongsTo(Job::class, 'job_id');
	}

    public function companies(){
	  	return $this->belongsTo(Company::class, 'company_id');
	}

	
}
