<?php

namespace App\Http\Middleware;

use Closure;

class CheckUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         if (auth('user')->check()) {
            if ($request->route()->getName() === 'auth.login') {
                return redirect()->route('profile');
            }

            return $next($request);
        } else {
            return redirect()->route('auth.login');
        }
    }
}
