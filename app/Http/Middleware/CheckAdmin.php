<?php

namespace App\Http\Middleware;

use Closure;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth('admin')->check()) {
            if ($request->route()->getName() === 'admin.login') {
                return redirect()->route('checkauth');
            }

            return $next($request);
        } else {
            return redirect()->route('admin.login');
        }
    }
}
