<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Job;

class JobcvResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'attributes' => [
                'id' => $this->id,
                'user_id' => $this->user_id,
                'job_id' => $this->job_id,
                'company_id' => $this->company_id,
                'users' => $this->users,
                'jobs' => new JobResource(Job::find($this->job_id)),
                'companies' => $this->companies,
                'created_at' => $this->created_at,
            ]
            
        ];
    }
}
