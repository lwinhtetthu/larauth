<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class JobResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'attributes' => [
                'id' => $this->id,
                'jobtitle' => $this->jobtitle,
                'companyname' => $this->companyname,
                'salary' => $this->salary,
                'location' => $this->location,
                'jobtime_id' => $this->jobtime_id,
                'jobtype' => $this->jobtype,
                'description' => $this->description,
                'requirement' => $this->requirement,
                'created_at' => $this->created_at,
                'company' => $this->company,
                'jobtypes' => $this->jobtypes,
                'jobtimes' => $this->jobtime,
            ]
            
        ];
    }
}
