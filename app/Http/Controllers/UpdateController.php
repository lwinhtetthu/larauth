<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Hash;
use App\Jobcv;
use App\Http\Resources\JobResource;
use App\Job;

class UpdateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id=auth('user')->id(); 
        // dd($id);
        $user = User::find($id);
        $jobcvs = Jobcv::where('user_id','=',$id)->get();

        if (count($jobcvs) == 0) {
            return view('user', compact('user'));
        } else {
            foreach ($jobcvs as $key => $value) {
                $jobid[] = $value['job_id'];
            }
            // dd($jobid);
            foreach ($jobid as $key => $value) {
                $job = Job::where('id','=',$value)->get();
                JobResource::withoutWrapping();
                $jobs[] = JobResource::collection($job);
            }
            
            foreach ($jobs as $key => $value) {
                foreach ($value as $k => $v) {
                    $var[] = $v;
                }
            }
            
            return view('user', compact('user','var'));
        }
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id=auth('user')->id(); 
        $user = User::find($id);

        return view('changepassword',compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'oldpassword' => 'required|string',
            'password' => 'required|string|confirmed',
            'password_confirmation' => 'required'
        ]);
        $userid = auth('user')->id();
        $dbpassword = User::select('password')->where('id','=',$userid)->get();
        foreach ($dbpassword as $userpassword) {
            $upass = $userpassword->password;
        }

        $newpassword = Hash::make($request->input('password'));
        $oldpassword = $request->input('oldpassword');
        
        if(Hash::check($oldpassword, $upass)){
        
            User::where('id','=', $userid)->update(
                array(
                    'password' =>  $newpassword,
                )
            );
            
            $request->session()->flush();

            return redirect()->route('auth.login');
        } else {
            return redirect()->back()->withErrors($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
