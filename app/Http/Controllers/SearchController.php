<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Http\Resources\JobResource;

class SearchController extends Controller
{
    public function Searchbox(Request $request){

    	$keyword = $request->input('searchjob');
    	
    	$finals = \App\Job::where([ 
        
        ['jobtitle', 'LIKE', '%' . $keyword . '%']
       	
       	])->get();

	    $jobtypes  = DB::table('jobtypes')->get(); 
	    $jobtimes    = DB::table('jobtimes')->get();	
	    return view('job',compact('finals','jobtypes','jobtimes'));
    }

    

    public function Searchbycate(Request $request){

    	$type = $request->input('jobtype_id');
    	$exp = $request->input('jobtime_id');
    	
    	if($type == "0" AND $exp == "0"){
    		$filters = \App\Job::all();
    	
    	} 
    	elseif($type !== "0" AND $exp == "0") 
    	{
    	$filters = \App\Job::where([

       	['jobtype','=', $type]

		])->get();

    	}	
    	elseif($type == "0" AND $exp !== "0")
    	{
    		{
    	$filters = \App\Job::where([

       	['jobtime_id','=', $exp]

		])->get();

    	}	
    	
    	} 
    	elseif($type !== "0" AND $exp !== "0")
    	{
    		
	    	$filters = \App\Job::where([

	       		['jobtime_id','=', $exp],
	       		['jobtype','=', $type]

			])->get();

    	}

		$jobtypes  = DB::table('jobtypes')->get(); 
	    $jobtimes    = DB::table('jobtimes')->get();	
	    return view('job',compact('filters','jobtypes','jobtimes', 'type','exp'));
    }
}
