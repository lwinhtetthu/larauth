<?php

namespace App\Http\Controllers;

use App\Job;
use DB;
use App\Company;
use Illuminate\Http\Request;
use App\Http\Resources\JobResource;

class AJobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $jobs = Job::get();
       // JobResource::withoutWrapping();
       //  $jobs = JobResource::collection(Job::all());o
       return view('admin.ajobdetail',compact('jobs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jobtimes = DB::table('jobtimes')->get();
        $jobtypes = DB::table('jobtypes')->get();
        $company = Company::get();
        return view('admin.ajob',compact('company','jobtimes','jobtypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'jobtitle'      => 'required',
            'companyname'   => 'required',
            'salary'        =>  'required',
            'location'      =>  'required',
            'jobtime_id'       =>  'required',      
            'jobtype'       =>  'required',
            'description'   =>  'required',
            'requirement'   =>  'required',
            'aboutcompany'  =>  'required',
            
     ]);

        


        Job::create([

            'jobtitle' =>$request->input('jobtitle'),
            'companyname' =>$request->input('companyname'),
            'salary' =>$request->input('salary'),
            'location'      =>$request->input('location'),
            'jobtime_id'       =>$request->input('jobtime_id'),
            'jobtype'       =>$request->input('jobtype'),
            'description'   =>$request->input('description'),
            'requirement'   =>$request->input('requirement'),
            'aboutcompany'  =>$request->input('aboutcompany'),
            

        ]);
        
        return redirect()->route('job.index');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function show(Job $job)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function edit(Job $job)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Job $job)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function destroy(Job $job)
    {
        //
    }
}
