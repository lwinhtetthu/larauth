<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;

class DownloadController extends Controller
{
public function download($filename) { 
// Check if file exists in storage directory


// $file_path = $_SERVER['DOCUMENT_ROOT'] .'/'.$filename; 
 
 $filedir = "../public/savecv/".$filename;
 // dd($filedir);
if ( file_exists( $filedir ) ) { 
	// dd($filedir);
	// Send Download 
	return Response::download( $filedir ); 
 
	} else { 
	return 'file not found';
	}
 }
}
