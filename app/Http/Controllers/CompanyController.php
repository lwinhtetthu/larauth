<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company = Company::get();
        return view('admin.companylist',compact('company'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.company');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[

            'companyname'   =>  'required',
            'email'         =>  'required|email',
            'phone'         =>  'required',
            'aboutcompany'  =>  'required',

        ]);
        $name = "logo.png";
        if ($request->hasFile('logo')) {
        
        $image = $request->file('logo');
        $name = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/logo');
        $image->move($destinationPath, $name);
          
        }   
        
        Company::create([
            'companyname' =>$request->input('companyname'),
            'email'       =>$request->input('email'),
            'phone'       =>$request->input('phone'),
            'aboutcompany'=>$request->input('aboutcompany'),
            'logo'        =>$name,    
        ]);

        return redirect()->route('company.index');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        
        return view('admin.companydetail',compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
