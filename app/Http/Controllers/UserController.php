<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\User;
use Auth;
use DB;

class UserController extends Controller
{
    public function __construct(){

        $this->middleware('user');
    }
    /**
     * publDisplay a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $id=auth('user')->id(); 
        $user = User::find($id);


        $jobtypes  = DB::table('jobtypes')->get(); 
        $levels    = DB::table('levels')->get();
        $locations = DB::table('locations')->get();

        
        

       
        return view('profile', compact('user', 'jobtypes', 'levels', 'locations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
      

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [

            'email' => 'required|email',
            'phone' => 'required',
            'firstname' => 'required',
            'lastname' => 'required',
            'title' => 'required',
            'jobtype_id' => 'required',
            'level_id' => 'required',
            'location_id' => 'required',
            'cv' => 'required',
            
        ]);

         if(Input::hasFile('cv'))
        {
       
        $destinationPath = "savecv/";
        $file= Input::file('cv');
        $filename = $file->getClientOriginalName();
        $upload = $file->move($destinationPath, $filename);
        
        }

        $data = array_filter(array_only($request->all(), [
                'email',
                'password',
                'firstname',
                'lastname',
                'phone',
                'title',
                'jobtype_id',
                'level_id',
                'location_id',
                'cv'


        ]));
        $data['cv'] = $filename;

        User::where('id',$id)->update($data);
        return redirect('profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         
    }
}
