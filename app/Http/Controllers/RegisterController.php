<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\User;
use DB;

class RegisterController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('profile');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jobtypes  = DB::table('jobtypes')->get(); 
        $levels    = DB::table('levels')->get();
        $locations = DB::table('locations')->get();
        return view('auth.register',compact('jobtypes','levels','locations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [

            'email' => 'required|email',
            'password' => 'required',
            'firstname' => 'required',
            'lastname' => 'required',
            'phone' => 'required',
            'title' => 'required',
            'jobtype_id' => 'required',
            'level_id' => 'required',
            'location_id' => 'required',
            'cv' => 'required',
        ]);
        
        if(Input::hasFile('cv'))
        {
        
        $destinationPath = "savecv/";
        $file= Input::file('cv');
        $reqcv = $request->cv;
        
        $file_name = $file->hashName();
        $str = explode(".",$file_name);

        $filename = $reqcv->storeAs(
            'savecv',
            $str[0].".docx"
        );

        
        
        
        
        $upload = $file->move($destinationPath, $filename);
        
        }


        $data = array_filter(array_only($request->all(), [
                'email',
                'password',
                'firstname',
                'lastname',
                'phone',
                'title',
                'jobtype_id',
                'level_id',
                'location_id',
                'cv'

        ]));
        $data['cv'] = $filename;

        User::create($data);

        return redirect('login');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
