<?php

namespace App\Http\Controllers\Auth\User;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    public function __construct()
    {
        $this->middleware('guest:user')->except(['logout']);
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\Response|void
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }


        if (auth('user')->attempt($this->credentials($request), $request->filled('remember'))) {
            // Go to
            return redirect()->intended(route('user'));
        } else {
            // error
            return redirect()->route('auth.login');
        }

        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function logout(Request $request)
    {
        if (!auth('user')->check()) {
            return redirect()->route('auth.login');
        }
        $this->guard()->logout();
        $request->session()->invalidate();

        return redirect()->route('home.index');
    }
}
