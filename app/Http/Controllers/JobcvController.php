<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Job;
use App\Jobcv;
use Illuminate\Http\Request;
use App\Http\Resources\JobcvResource;

class JobcvController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        JobcvResource::withoutWrapping();
        $jobcvs = JobcvResource::collection(Jobcv::all());

        // return $jobcvs;
        return view('admin.jobcv',compact('jobcvs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id)
    {
        
        $company = Job::where('id','=',$id)->select('companyname')->get();
        foreach ($company as $comp) {
           $com = $comp->companyname; 
        }

        $user =auth('user')->id();
        
        Jobcv::create([
                
            'user_id'       =>$user,
            'job_id'        =>$id,
            'company_id'    =>$com,


        ]);

        return view('finish');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Jobcv  $jobcv
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $jobcv = Jobcv::where('id','=',$id)->get();
        JobcvResource::withoutWrapping();
        $jobcvs = JobcvResource::collection($jobcv);
        // return $jobcvs;
        return view('admin.detailjob',compact('jobcvs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Jobcv  $jobcv
     * @return \Illuminate\Http\Response
     */
    public function edit(Jobcv $jobcv)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Jobcv  $jobcv
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Jobcv $jobcv)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Jobcv  $jobcv
     * @return \Illuminate\Http\Response
     */
    public function destroy(Jobcv $jobcv)
    {
        //
    }
}
