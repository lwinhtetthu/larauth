<?php

namespace App\Http\Controllers;

use DB;
use App\Job;
use App\Company;
use Illuminate\Http\Request;
use App\Http\Resources\JobResource;
use App\Http\Resources\CompanyResource;

class JobController extends Controller
{
     
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       

        JobResource::withoutWrapping();
        $jobs = JobResource::collection(Job::all());
        
        $jobtypes  = DB::table('jobtypes')->get(); 
        $jobtimes    = DB::table('jobtimes')->get();
        return view('job',compact('jobs','jobtypes','jobtimes'));
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
       
        $job = Job::where('id','=',$id)->get();
        JobResource::withoutWrapping();
        $jobs = JobResource::collection($job);
       
        return view('jobdetail',compact('jobs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
