<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// admin login and register
Route::namespace('Auth\Admin')->group(function () {
    Route::get('admin/login', 'LoginController@showLoginForm')->name('admin.login');
    
    Route::post('admin/login', 'LoginController@login')->name('admin.login.submit');
    
    Route::post('admin/logout', 'LoginController@logout')->name('admin.logout');
});
Route::get('/admin/register', 'AregisterController@create')->name('admin.register');
Route::post('/save', 'AregisterController@store');
Route::get('/admin', 'ALoginController@index')->name('admin');
Route::get('/view', 'ALoginController@create')->name('view');
Route::resource('job','AJobController');
Route::resource('company','CompanyController');
Route::get('/detail/{id}','JobcvController@show')->name('detail');



// user login and signup
Route::namespace('Auth\User')->group(function () {
    Route::get('/login', 'LoginController@showLoginForm')->name('auth.login');  
    Route::post('/login', 'LoginController@login')->name('login.submit');
    Route::post('/logout', 'LoginController@logout')->name('auth.logout');
});

Route::get('/register', 'RegisterController@create')->name('register');
Route::post('/post', 'RegisterController@store');


Route::resource('/', 'UserController');
Route::get('/save/{filename}', 'DownloadController@download')->name('save.cv');




// User update
Route::get('user','UpdateController@index')->name('user');
Route::get('user/profile','UserController@index')->name('profile');
Route::patch('user/profile/{id}','UserController@update');

Route::get('user/update','UpdateController@create')->name('edit');
Route::patch('/user/update/{id}','UpdateController@update')->name('update');

//Job
Route::resource('search','JobController');
Route::get('jobcv/{id}','JobcvController@store')->name('jobcv.store');
Route::get('/jobapply','JobcvController@index')->name('jobcv.index');
//Home

Route::resource('home','HomeController');

// SearchBox
Route::post('/searchjob','SearchController@Searchbox');

Route::get('/searchbycate','SearchController@searchbycate');