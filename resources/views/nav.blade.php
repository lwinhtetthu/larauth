<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <img src="/images/za.png" width="50px" height="50px">&nbsp;
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a style="color: #8e000E;" class="btn btn-light" href="{{ route('home.index') }}">Home</a>
      </li>
      <li class="nav-item">
        <a style="color: #8e000E;" class="btn btn-light" href="{{ route('auth.login') }}">Login</a>
      </li>
      <li class="nav-item">
        <a style="color: #8e000E;" class="btn btn-light" href="{{ route('register') }}">Register</a>
      </li>
      <li class="nav-item">
        <a style="color: #8e000E;" class="btn btn-light" href="{{ route('search.index') }}">Search Job</a>
      </li>
      
    </ul>
    <form class="form-inline my-2 my-lg-0" method="POST" action="/searchjob">
      @csrf
      <input class="form-control mr-sm-2" name="searchjob" type="search" placeholder="Search" aria-label="Search">
      <button style="color: #8e000E;" class="btn btn-light my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>