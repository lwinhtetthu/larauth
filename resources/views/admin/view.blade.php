@extends('admin.adminmaster')

@section('content')

<div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">CV-List</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
<div class="row">

<br>         

<table class="table table-striped">
	<tr class="info">
		<td>ID</td>
		<td>Email</td>
		<td>First-Name</td>
		<td>Last-Name</td>
		<td>Phone</td>
		<td>Title</td>
		<td>JobType</td>
		<td>Level</td>
		<td>Location</td>
		<td>CV-file</td>
	</tr>
@foreach($users as $user)
	<tr class="active">
		<td>{{ $user['id'] }}</td>
		<td>{{ $user['email'] }}</td>
		<td>{{ $user['firstname'] }}</td>
		<td>{{ $user['lastname'] }}</td>
		<td>{{ $user['phone'] }}</td>
		<td>{{ $user['title'] }}</td>
		<td>{{ $user['jobtype_id'] }}</td>
		<td>{{ $user['level_id'] }}</td>
		<td>{{ $user['location_id'] }}</td>
		
		
		<td><a href="{!! route('save.cv', $user->cv) !!}" target="_blank"> Click to download CV </a></a></td>
	
	</tr>
@endforeach

</table>
               
            </div>
            <!-- /#page-wrapper -->

        </div>

@endsection

