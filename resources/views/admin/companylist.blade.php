@extends('admin.adminmaster')

@section('content')

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Company List</h1>
            <a href="{{ route('company.create') }}" class="btn btn-lg btn-success " style="height: 50px;"/>Insert Company</a>
        </div>
           
               
    </div>
    
    <br>
    
        <div class="row">
            <div class="col-md-12">
            
        @foreach($company as $comp)


       
            <div class="col-md-4">
             <div class="thumbnail">
               <img src="/logo/{{ $comp->logo }} " style="width: 150px; height: 150px;" >
                <div class="caption">
                 <h3 style="text-align: center">{{ $comp->companyname }}
                    <p>
                        <a href="{{ route('company.show', $comp->id) }}" class="btn btn-primary" role="button">View</a>
                    
                        <a href="#" class="btn btn-primary" role="button">Edit</a>
                    
                        <a href="#" class="btn btn-primary" role="button">Delete</a>


                    </p>
                </h3>
                  
                        
                    </div>
                    </div>
                </div>
        
                
        @endforeach
       {{-- </div>  --}}
       </div>
    </div>
    
  <hr> 
    

            
  






</div>

@endsection