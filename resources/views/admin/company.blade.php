@extends('admin.adminmaster')

@section('content')

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Insert Company Info</h1>
        </div>
                    
    </div>
<form action=" {{ route('company.store') }} " method="post" class="container" style="width: 600px; background-color: #ECEFEB" enctype="multipart/form-data">
    {{ csrf_field() }}
<br>
    <div class="wrap-input form-group">
        <label for="companyname">Company Name:</label>
                    
        <input type="text" name="companyname" maxlength="50" id="companyname" class="form-control" />
               
    </div>
    
    <div class="wrap-input form-group">
        <label for="email">Email Address:</label>

        <input name="email" type="text" class="form-control" id="email" />
    </div>
                
    
    <div class="wrap-input form-group">
        <label for="phone">Phone:</label>
                               
        <input name="phone" type="text" id="phone" class="form-control" />
    </div>


    <div class="wrap-input form-group">
        <label for="aboutcompany">About Company:</label>

        <textarea name="aboutcompany" id="aboutcompany" class="form-control"></textarea>
    </div>

    <div class="wrap-input form-group">
        <div>
            <label>Company Logo</label>

        </div>

        <div style="display: inline-block; width: 100%;">
            <input type="file" name="logo" id="logo" class="form-control" />

        </div>
    </div>

                
    <div class="wrap-input">
                    
            <input type="submit" value="Finish" class="btn btn-lg btn-success btn-block" style="width: 100%; height: 50px; cursor: pointer" />
    

            @include('errors')
                    
    </div>
<br>
</form>


</div>

@endsection