@extends('admin.adminmaster')

@section('content')

<div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Applyed User</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
<div class="row">

<br>         

<table class="table table-striped">
	<tr class="info">
		<td>ID</td>
		<td>Email</td>
		<td>Job Name</td>
		<td>Company Name</td>
		<td>Download CV</td>
	</tr>
@foreach($jobcvs as $jobcv)
	<tr class="active">
		<td>{{ $jobcv->id }}</td>
		<td>{{ $jobcv->users['email'] }}</td>
		<td>{{ $jobcv->jobs['jobtitle'] }}</td>
		<td>{{ $jobcv->companies['companyname'] }}</td>
		
		<td><a href="{!! route('save.cv', $jobcv->users['cv']) !!}" target="_blank"> Click to download CV </a></a></td>

		<td><a href="{{ route('detail',$jobcv->id) }}" class="btn btn-info">Detail</a></td>
	
	</tr>
@endforeach

</table>
               
            </div>
            <!-- /#page-wrapper -->

        </div>

@endsection

