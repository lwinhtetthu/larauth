@extends('admin.adminmaster')

@section('content')

<div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Detail Info</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
<div class="row">

<br>         


@foreach($jobcvs as $jobcv)
	<div>                
    <label><strong>Email: </strong> {{ $jobcv->users['email'] }} </label>
    </div>  
    
    <div>    
    <label><strong>Firstname:  </strong> {{ $jobcv->users['firstname'] }} </label>           
    </div>

    <div>    
    <label><strong>Lastname:  </strong> {{ $jobcv->users['lastname'] }} </label>           
    </div>

    <div>    
    <label><strong>Phone:  </strong> {{ $jobcv->users['phone'] }} </label>           
    </div>

    <div>    
    <label><strong>Title:  </strong> {{ $jobcv->users['title'] }} </label>           
    </div>

    <div>    
    <label><strong>JobType:  </strong> {{ $jobcv->jobs['jobtypes']->jobtypes }} </label>           
    </div>

    <div>    
    <label><strong>Job Type/Time:  </strong> {{ $jobcv->jobs['jobtime']->jobtime }} </label>           
    </div>

    <div>    
    <label><strong>Location:  </strong> {{ $jobcv->users['title'] }} </label>           
    </div>
		
		
		<a href="{!! route('save.cv', $jobcv->users['cv']) !!}" target="_blank"> Click to download CV </a>
	
@endforeach

</table>
               
            </div>
            <hr>
      	@foreach( $jobcvs as $jobcv )
                        
        <div class="row">
               
            <div class="col">   
                    <img src="/logo/{{ $jobcv->companies['logo'] }}" width="140px" height="140px" style="float: right;">
                     
            </div>
            &nbsp;&nbsp;
            
            <div class="col-md-10">
                

                <div class="">
                    <h3 style="color: blue">{{ $jobcv->jobs['jobtitle'] }} *</h3>
                </div>

                <div class="">
                    <h4 style="color: green">{{ $jobcv->companies['companyname'] }} </h4>
                </div>

                <div>
                    
                    <label><strong>Salary: </strong> {{ $jobcv->jobs['salary'] }} </label>
                </div>
                <div>
                    
                    <label><strong>Location:  </strong> {{ $jobcv->jobs['location'] }} </label>
                
                </div>

                <div>
                    
                    <label><strong>Job Time/Type:  </strong> {{ $jobcv->jobs['jobtime']->jobtime }} </label>
                
                </div>
                
            			
                <div>
                    <label><strong>Job Function:  </strong> {{ $jobcv->jobs['jobtypes']->jobtypes }} </label>
               
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    

                    <label><strong>Posted:  </strong> {{ $jobcv->jobs['created_at'] }} </span></label>
                <hr>
                </div>
                                
                   
            </div>
                     
            

          </div>  
        
        @endforeach 

        </div>

@endsection

