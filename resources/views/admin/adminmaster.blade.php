<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin Dashboard</title>

    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Exo">
    <!-- Bootstrap Core CSS -->
    <link href="/admindashboard/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="/admindashboard/css/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="/admindashboard/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/admindashboard/css/startmin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="/admindashboard/css/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/admindashboard/css/font-awesome.min.css" rel="stylesheet" type="text/css">

     

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond./admindashboard/js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <img src="/images/za.png" width="45px" height="45px">&nbsp;
</a>
        </div>

        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

        <!-- Top Navigation: Left Menu -->


        <!-- Top Navigation: Right Menu -->
        
        <ul class="nav navbar-right navbar-top-links">
            
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color: lightgreen;">
                    <i class="fa fa-user fa-fw"></i> Admin <b class="caret"></b>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    
                    
                    

                    <li class="nav-item active">
                         <a class="nav-link" href="#" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit()"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        
                        <form action="{{ route('admin.logout') }}" id="logout-form" method="POST" style="display: none">
                            {{ csrf_field() }}
                        </form>
                    </li>

                </ul>
            </li>
        </ul>

        <!-- Sidebar -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">

                <ul class="nav" id="side-menu">
                    
                    
                   
                    <li>
                        <a href="{{ route('admin') }}"><i class="fa fa-dashboard fa-fw"></i> | Home</a>
                    </li>
                    
                    <li>
                        <a href="{{ route('view') }}"><i class="fa fa-list fa-fw"></i> | CV-Lists</a>
                    </li>
                    
                    <li>
                        <a href="{{ route('job.index') }}"><i class="fa fa-suitcase fa-fw"></i> | Job</a>
                    </li>

                    <li>
                        <a href="{{ route('company.index') }}"><i class="fa fa-building fa-fw"></i> | Company</a>
                    </li>

                    <li>
                        <a href="{{ route('jobcv.index') }}"><i class="fa fa-black-tie fa-fw"></i> | User Applyed</a>
                    </li>
                    
                </ul>

            </div>
        </div>
    </nav>

    <!-- Page Content -->
</div>
@yield('content')

<!-- jQuery -->
        <script src="/admindashboard/js/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="/admindashboard/js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="/admindashboard/js/metisMenu.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="/admindashboard/js/startmin.js"></script>

</body>
</html>
