@extends('admin.adminmaster')


@section('content')


<div id="page-wrapper">
       <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Job List</h1>
            <a href="{{ route('job.create') }}" class="btn btn-lg btn-success " style="height: 50px;"/>Insert A New Job</a>
        </div>
           
               
    </div>     
                
    @foreach( $jobs as $job )
                        
        <div class="row">
               
            <div class="col">   
                    <img src="/logo/{{ $job->company['logo'] }}" width="140px" height="140px" style="float: right;">
                     
            </div>
            &nbsp;&nbsp;
            
            <div class="col-md-10">
                

                <div class="">
                    <h3 style="color: blue">{{ $job->jobtitle }} *</h3>
                </div>

                <div class="">
                    <h4 style="color: green">[ {{ $job->company['companyname'] }} ]</h4>
                </div>

                <div>
                    
                    <label><strong>Salary: </strong>[ {{ $job->salary }} ]</label>
                </div>
                <div>
                    
                    <label><strong>Location:  </strong>[ {{ $job->location }} ]</label>
                
                </div>
                
            
                <div>
                    <label><strong>Job Function:  </strong>[ {{ $job->jobtypes['jobtypes'] }} ]</label>
                
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    

                    <label><strong>Posted:  </strong>[ {{ $job->created_at }} ]</span></label>
                
                </div>
                                
                <button class="btn btn-primary">Edit</button>
                <button class="btn btn-primary">Delete</button>     
            </div>
                     
            

          </div>  
        <hr style="border-width: 30px;">
        @endforeach                   
            
                
    </div>

@endsection