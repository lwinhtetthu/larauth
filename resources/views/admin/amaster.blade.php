<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>FindOne</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Exo">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

    

    <!-- Custom styles for this template -->
    <link href="css/layout.css" rel="stylesheet">
</head>


<header>

    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark" style=" height: 65px;">
        <img src="/images/za.png" width="50px" height="50px">&nbsp;

        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto" >

                &nbsp; &nbsp;

                @if(auth('admin')->user())
                    <li class="nav-item active" style="padding-left: 860px;">
                        <a class="nav-link btn btn-success" href="#" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit()">Logout </a>
                        <form action="{{ route('admin.logout') }}" id="logout-form" method="POST" style="display: none">
                            {{ csrf_field() }}
                        </form>
                    </li>
                @endif
            </ul>

            

        </div>
    </nav>

</header>

<body style="background-color: #F5F5F5;">
@yield('content')
</body>



</html>
