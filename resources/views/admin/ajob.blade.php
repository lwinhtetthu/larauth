@extends('admin.adminmaster')

@section('content')

<div id="page-wrapper">
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-header">Insert A New JOB</h1>
        </div>
                    
    </div>
  <div class="row">

<form action=" {{ route('job.store') }} " method="post" class="container" style="width: 600px; background-color: #ECEFEB" enctype="multipart/form-data">
    {{ csrf_field() }}
<br>
    <div class="wrap-input form-group">
        <label for="jobtitle">JobTitle:</label>
                    
        <input type="text" name="jobtitle" maxlength="50" id="jobtitle" class="form-control" />
               
    </div>
    
    <div class="wrap-input form-group">
        <label for="companyname">CompanyName:</label>

         <select name="companyname" id="companyname" class="sign-up-select form-control">
                
                <option value="0"></option>
                @foreach($company as $comp)

                <option value="{{ $comp->id }}">{{ $comp->companyname }}</option>

                @endforeach
        </select>
    </div>
                
    
    <div class="wrap-input form-group">
        <label for="salary">Salary:</label>
                               
        <input name="salary" type="text" id="salary" class="form-control" />
    </div>


    <div class="wrap-input form-group">
        <label for="location">Location:</label>
                               
        <input name="location" type="text" id="location" class="form-control" />
    </div>
                
    
    <div class="wrap-input form-group">
        <label for="jobtime_id">Job Type/Time:</label>
        <select name="jobtime_id" id="jobtime_id" class="sign-up-select form-control">
                
                <option value="0"></option>
                    @foreach($jobtimes as $jobtime)
                    <option value="{{ $jobtime->id }}">{{ $jobtime->jobtime }}</option>
                    @endforeach

        </select>
    </div>

    <div class="wrap-input form-group">
        <label for="jobtype">Job Function</label>
        <select name="jobtype" id="jobtype" class="sign-up-select form-control" >

                    <option value="0"></option>
                    @foreach($jobtypes as $jobtype)
                    <option value="{{ $jobtype->id }}">{{ $jobtype->jobtypes }}</option>
                    @endforeach

        </select>
        
    </div>
                

    <div class="wrap-input form-group">
        <label for="description">Job Description:</label>

        <textarea name="description" id="description" class="form-control"></textarea>
    </div>

    <div class="wrap-input form-group">
        <label for="requirement">Job Requirement:</label>

        <textarea name="requirement" id="requirement" class="form-control"></textarea>
    </div>

    <div class="wrap-input form-group">
        <label for="aboutcompany">About Company:</label>

        <textarea name="aboutcompany" id="aboutcompany" class="form-control"></textarea>
    </div>

                
    <div class="wrap-input">
                    
            <input type="submit" value="Insert" class="btn btn-lg btn-success btn-block" style="width: 100%; height: 50px; cursor: pointer" />
    

            @include('errors')
                    
    </div>
<br>
</form>


      
  </div>  
</div>

@endsection