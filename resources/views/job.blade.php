@extends('layout')


@section('content')
<style type="text/css">
    
    @media screen and (max-width: 767px){
        .myfilter{
            width: 80%;
        }
    @media screen and (max-width: 767px){
        .myform{
            width: 90%;
        }
    }
</style>
<body style="background-color: #ECEDF0;">
    
    <div class="row" style="width: 100%">
    <div class="col-sm-1"></div>
    
    
    <form class="container myfilter form-control col-sm-2" method="get" action="/searchbycate" style="margin-top: 60px; height: 330px;">
         <span style="color: #8e000E">Filter by: </span>   
         <hr>

         <div>
            <span style="margin-top: 10px;">Search By Job Function :</span>
            <select style="margin-top: 10px;" name="jobtype_id" id="jobtype" class="sign-up-select form-control" >

                        <option value="0" @if(isset($type)) 
                        @if($type == "0") selected @endif @endif>All</option>
                        @foreach($jobtypes as $jobtype)
                        <option value="{{ $jobtype->id }}" 
                            @if(isset($type))
                            @if($type == $jobtype->id) selected 
                            @endif
                            @endif>{{ $jobtype->jobtypes }}</option>
                        @endforeach
                        
            </select>
         </div>
         <hr>

         <div>
             <span style="margin-top: 10px;">Search By Job Type :</span>
             <select style="margin-top: 10px;" name="jobtime_id" id="jobtime" class="sign-up-select form-control">
                    
                    <option value="0" @if(isset($exp))
                    @if($exp == "0") selected @endif @endif selected>All</option>
                    @foreach($jobtimes as $jobtime)
                    <option value="{{ $jobtime->id }}" 
                        @if(isset($exp))
                        @if($exp == $jobtime->id) selected 
                        @endif
                        @endif>
                        {{ $jobtime->jobtime }}</option>
                    @endforeach
                    

            </select>
         </div>
         <hr>
         
         
             <span style="margin-left: 20px;">Search :</span><button type="submit" class="btn btn-outline-primary" style="margin-left: 25px;">Apply</button>
         
    </form>


    
    <div class="container myform form-control col-sm-8" style="margin-top: 60px;">
    
    @if(isset($jobs))        
                
    @foreach( $jobs as $job )
                        
        <div class="row" style="width: 100%">
               
            <div class="col">   
                    <img src="/logo/{{ $job->company['logo'] }}" class="form-control" style="float: right;">
                     
            </div>
            &nbsp;&nbsp;&nbsp;
            
            <div class="col-sm-10">
                

                <div class="">
                    <h4 style="color: blue">{{ $job->jobtitle }} *</h4>
                </div>

                <div class="">
                    <h5 style="color: darkblue">[ {{ $job->company['companyname'] }} ]</h5>
                </div>

                <div>
                    
                    <label><strong>Salary: </strong>[ {{ $job->salary }} ]</label>
                </div>
                <div>
                    
                    <label><strong>Location:  </strong>[ {{ $job->location }} ]</label>
                   </div>
                   <div>
                    <label><strong>Job Time/Type:  </strong>
                        <span>
                        [ {{ $job->jobtime['jobtime'] }} ]
                        </span>
                    </label>
                </div>
                
            
                <div>
                    <label><strong>Job Function:  </strong>[ {{ $job->jobtypes['jobtypes'] }} ]</label>
                
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    

                    <label><strong>Posted:  </strong>[ {{ $job->created_at }} ]</span></label>
                
                </div>
                                

                <a href="{{ route('search.show', $job->id) }}" class="btn btn-info">View Details</a>  
                @if(auth('user')->user())                       
                <a href=" {{ route('jobcv.store', $job->id) }} " class="btn btn-success">Apply Now</a>
                
                @else

                <a href="{{ route('auth.login') }}" style="color: blue">$Login to Apply</a>    
                
                @endif   
            </div>
                     
            



          </div>  
        <hr style="border-width: 15px;">
        @endforeach                   
        @elseif(isset($finals))    
        
        @foreach( $finals as $final )
                        
        <div class="row" style="width: 100%">
               
            <div class="col">   
                    <img src="/logo/{{ $final->company['logo'] }}" class="form-control" style="float: right;">
                     
            </div>
            &nbsp;&nbsp;&nbsp;
            
            <div class="col-md-10">
                

                <div class="">
                    <h4 style="color: blue">{{ $final->jobtitle }} *</h4>
                </div>

                <div class="">
                    <h5 style="color: darkblue">[ {{ $final->company['companyname'] }} ]</h5>
                </div>

                <div>
                    
                    <label><strong>Salary: </strong>[ {{ $final->salary }} ]</label>
                </div>
                <div>
                    
                    <label><strong>Location:  </strong>[ {{ $final->location }} ]</label>
                </div>
                <div>
                    <label><strong>Experience Level:  </strong>[ {{ $final->jobtime['jobtime'] }} ]</span></label>
                </div>
                
            
                <div>
                    <label><strong>Job Function:  </strong>[ {{ $final->jobtypes['jobtypes'] }} ]</label>
                
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    

                    <label><strong>Posted:  </strong>[ {{ $final->created_at }} ]</span></label>
                
                </div>
                                

                <a href="{{ route('search.show', $final->id) }}" class="btn btn-info">View Details</a>     
                @if(auth('user')->user())                       
                <a href=" {{ route('jobcv.store', $final->id) }} " class="btn btn-success">Apply Now</a>
                
                @else

                <a href="{{ route('auth.login') }}" style="color: blue">$Login to Apply</a>    
                
                @endif   
            </div>
                     
            



          </div>  
        <hr style="border-width: 15px;">
        @endforeach 
        @elseif(isset($filters))

        @foreach( $filters as $filter )
        <div class="row" style="width: 100%">
               
            <div class="col">   
                    <img src="/logo/{{ $filter->company['logo'] }}" class="form-control" style="float: right;">
                     
            </div>
            &nbsp;&nbsp;&nbsp;
            
            <div class="col-md-10">
                

                <div class="">
                    <h4 style="color: blue">{{ $filter->jobtitle }} *</h4>
                </div>

                <div class="">
                    <h5 style="color: darkblue">[ {{ $filter->company['companyname'] }} ]</h5>
                </div>

                <div>
                    
                    <label><strong>Salary: </strong>[ {{ $filter->salary }} ]</label>
                </div>
                <div>
                    
                    <label><strong>Location:  </strong>[ {{ $filter->location }} ]</label>
                 </div>
                 <div>
                    <label><strong>Experience Level:  </strong>[ {{ $filter->jobtime['jobtime'] }} ]</span></label>
                </div>
                
            
                <div>
                    <label><strong>Job Function:  </strong>[ {{ $filter->jobtypes['jobtypes'] }} ]</label>
                
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    

                    <label><strong>Posted:  </strong>[ {{ $filter->created_at }} ]</span></label>
                
                </div>
                                

                <a href="{{ route('search.show', $filter->id) }}" class="btn btn-info">View Details</a>     
            @if(auth('user')->user())                       
                <a href=" {{ route('jobcv.store', $filter->id) }} " class="btn btn-success">Apply Now</a>
                
                @else

                <a href="{{ route('auth.login') }}" style="color: blue">$Login to Apply</a>    
                
                @endif   
            </div>
                     
            



          </div>  
        <hr style="border-width: 15px;">
        @endforeach
        @endif        
    </div>
    

    <div class="col-sm-1"></div>

    </div>
</body>
@endsection