
@extends('master')

@section('content')
<style type="text/css">
    .myregister{
        width: 500px;
    }

    @media screen and (max-width: 767px){
        .myregister{
            width: 100%;
        }
    }
</style>
<body style="background-color: #ECEDF0;">    
<form action="/post" method="post" class="container form-control myregister" style="background-color: white; margin-top: 60px;" enctype="multipart/form-data">
    {{ csrf_field() }}
    
    
    <h4 style="text-align: center; padding-top: 20px;">Sign Up Now And Move Up Today</h4>
    
    <hr>
    <div class="wrap-input form-group">
        <label for="email"><strong>Email:</strong></label>
                    
        <input type="text" name="email" maxlength="50" id="email" class="form-control" autofocus />
               
    </div>
    
    <div class="wrap-input form-group">
        <label for="password"><strong>Password:</strong></label>

        <input name="password" type="password" maxlength="18" class="form-control" minlength="4" />
    </div>
                
    
    <div class="wrap-input form-group">
        <label for="firstname"><strong>First Name:</strong></label>
                               
        <input name="firstname" type="text" maxlength="50" id="firstname" class="form-control" />
    </div>


    <div class="wrap-input form-group">
        <label for="lastname"><strong>Last Name:</strong></label>
                               
        <input name="lastname" type="text" maxlength="50" id="lastname" class="form-control" />
    </div>
                

    <div class="wrap-input form-group">
        <label for="phone"><strong>Phone Number:</strong></label>
        
        <input name="phone" type="text" maxlength="20" id="phone"  class="form-control" placeholder="+95 1 502 077" />
    </div>
                
    
    <div class="wrap-input form-group">
        <label for="title"><strong>Job Title:</strong></label>
        
        <input name="title" type="text" maxlength="50" id="title" class="form-control" placeholder="Job Title" />
    </div>
                

    <div class="wrap-input form-group">
        <label for="jobtype"><strong>Your Primary Job Function</strong></label>
        <select name="jobtype_id" id="jobtype" class="sign-up-select form-control" >

                    <option value="0">Choose Job Function</option>
                    @foreach($jobtypes as $jobtype)
                    <option value="{{ $jobtype->id }}">{{ $jobtype->jobtypes }}</option>
                    @endforeach

        </select>
        
    </div>
                
                
    <div class="wrap-input form-group">
        <label for="level"><strong>Your Experience Level</strong></label>
        <select name="level_id" id="level" class="sign-up-select form-control">
                
                <option value="0">Choose Experience Level</option>
                @foreach($levels as $level)
                <option value="{{ $level->id }}">{{ $level->levels }}</option>
                @endforeach
                

        </select>
    </div>
                

    <div class="wrap-input form-group">
        <label for="location"><strong>Your Current Location</strong></label>
        <select name="location_id" id="location" class="sign-up-select form-control">
                <option value="0">Choose Location</option>
                @foreach($locations as $location)
                    <option value="{{ $location->id }}">{{ $location->locations }}</option>
                @endforeach

        </select>
                                
    </div>

                
    <div class="wrap-input form-group div-sign-up-upload-cv form-control">
        <div>
            <label><strong>Upload your latest CV and Apply from anywhere</strong></label>
            
        </div>
        
        <div style="display: inline-block; width: 100%;">        
            <input type="file" name="cv" id="cv" />
            
        </div>
    </div>
                        
                
    <div class="wrap-input">
                    
            <input type="submit" value="Register" class="btn btn-lg btn-info btn-block" style="width: 100%; height: 50px; cursor: pointer" />
    

            @include('errors')
                    
    </div>
<br>
</form>

</body>

              
@endsection
