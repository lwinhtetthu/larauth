@extends('master')

@section('content')
<style type="text/css">
    .mylogin{
        width: 500px;
    }

    @media screen and (max-width: 767px){
        .mylogin{
            width: 100%;
        }
    }
</style>
<body style="background-color: #ECEDF0;">
<div class="container mylogin">


<form action="{{ route('login.submit') }}" method="post" class="container form-control" style="background-color: white; margin-top: 60px; ">
{{ csrf_field() }}
    
     
       <h4 style="text-align: center; ">Login To Your Profile</h4> 
       <hr>
        <div class="form-group">
            
        <label for="email"><strong>Email:</strong></label>
           
        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

        @if ($errors->has('email'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
            
        </div>
        
 
        <div class="form-group">
            <label for="password"><strong>Password:</strong></label>

            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

            @if ($errors->has('password'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>
        
        <div class="checkbox">
        <label>
            <input name="remember" type="checkbox" value="Remember Me">Remember Me
        </label>
        </div>
        
        <div class="form-group">
            <button style="cursor:pointer" type="submit" class="btn btn-lg btn-dark btn-block" >Login</button>
        </div>

        <span style="font-size: 15px;">Don't have an account?</span>

        <div class="form-group">
            <a href="{{ route('register') }}" style="cursor:pointer" class="btn btn-lg btn-info btn-block" >Register</a>
        </div>
@include('errors')
</form>
<br>
<br>
<br>
</div>

</body>


@endsection