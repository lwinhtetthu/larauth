
@extends('layout')


@section('content')

<body style="background-color: #ECEDF0;">
    <div class="container form-control" style="margin-top: 85px;">
            
                
    @foreach( $jobs as $job )
                        
        <div class="row">
               
            <div class="col">   
                    <img src="/logo/{{ $job->company['logo'] }}"  height="140px" class="form-control" style="float: right;">
                     
            </div>
            &nbsp;&nbsp;&nbsp;
            
            <div class="col-md-10">
                

                <div class="">
                    <h3 style="color: #8e000E">{{ $job->jobtitle }} *</h3>
                </div>

                <div class="">
                    <h4 style="color: blue">[ {{ $job->company['companyname'] }} ]</h4>
                </div>

                <div>
                    
                    <label><strong>Salary: </strong>[ {{ $job->salary }} ]</label>
                </div>
                <div>
                    
                    <label><strong>Location:  </strong>[ {{ $job->location }} ]</label>
                
                </div>
                
            
                <div>
                    <label><strong>Job Function:  </strong>[ {{ $job->jobtypes['jobtypes'] }} ]</label>
                
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    

                    <label><strong>JobTime:  </strong>[ {{ $job->jobtime['jobtime'] }} ]</span></label>
                
                </div>
                
                <div>
                    
                    <label><strong>Description:  </strong> <br>{{ $job->description }} </label>
                
                </div> 

                 <div>
                    
                    <label><strong>Requirement:  </strong> <br>{{ $job->requirement }} </label>
                
                </div> 

                 <div>
                    
                    <label><strong>About Company:  </strong> <br>{{ $job->aboutcompany }} </label>
                
                </div> 
            @if(auth('user')->user())                       
            <a href=" {{ route('jobcv.store', $job->id) }} " class="btn btn-success">Apply Now</a>
            
            @else

            <a href="{{ route('auth.login') }}" style="color: blue">$Login to Apply</a>    
            
            @endif
            </div>
                     
            
          </div>  
        <hr style="border-width: 30px;">
        @endforeach                   
            
                
    </div>
</body>
@endsection