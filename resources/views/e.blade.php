<!DOCTYPE HTML>
<!--
    Synchronous by TEMPLATED
    templated.co @templatedco
    Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
    <head>
        <title>ZA</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="footer, contact, form, icons" />
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
        {{-- <script src="/template/js/html5shiv.js"></script> --}}
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="/template/js/skel.min.js"></script>
        <script src="/template/js/skel-panels.min.js"></script>
        <script src="/template/js/init.js"></script>

        
        <link rel="stylesheet" href="footer/footer.css">

        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

        <link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
        
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
        
     {{-- <noscript> --}}
         <link rel="stylesheet" href="/template/css/skel-noscript.css" />
     {{-- </noscript> --}}
        
        <link rel="stylesheet" type="text/css" href="/template/css/style-desktop.css">
        <link rel="stylesheet" href="/template/css/style.css" />

        <!-- Bootstrap core CSS -->
       
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
              integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

              <!-- Latest compiled and minified CSS -->


        
        
        {{-- <link rel="stylesheet" href="/template/css/ie/v8.css" /> --}}
      
      {{-- <link rel="stylesheet" href="/template/css/ie/v9.css" /> --}}
    </head>

    @include('nav')
    <body class="homepage">
        <div id="wrapper">
            
            <!-- Header -->
            <div id="header">
                <div class="container"> 
                    
                    

                    <!-- Logo -->
                    <div id="logo">
                        <img src="/images/za.png" width="150px" height="150px">
                    </div>
                    
                    <!-- Nav -->
                   {{--  <nav id="nav">
                        <ul>
                            <li class="active"><a href="index.html">Homepage</a></li>
                            <li><a href="{{ route('auth.login') }}">User Login</a></li>
                            <li><a href="{{ route('register') }}"">Registration</a></li>
                            <li><a href="{{ route('search.index') }}">Search Job</a></li>
                            <li><a href="onecolumn.html">Employer</a></li>
                        </ul>
                    </nav> --}}
                </div>
            </div>
            <!-- /Header -->
            
            <div id="page">
                <div class="container">
                    <div class="row">
                        <div class="3u">
                            
                            <section id="sidebar1">
                                <header>
                                    <h2>Companies</h2>
                                </header>

                                
                                @foreach( $companies as $com )
                                <div style="text-align:center" class="form-control">
                                <a href=""><img src="/logo/{{ $com->logo }}" width="90px;" height="90px"></a>
                                </div>
                                    
                                
                                @endforeach;
                                
                            
                            </section>
                        </div>
                        <div class="6u skel-cell-important">
                            <section id="content" >
                                <header style="text-align: center">
                                    <h2>Jobs</h2>
                                </header>
                                

                                
                               
                                <div class="row">
                                @foreach( $jobs as $job )
                                 
                                 
                                
                                <div class="col-md-6" style="width: 800px;">  
                                
                                    
                                
                                <h3><a  href="{{ route('search.show', $job->id) }}"><strong style="color: #8e000E; font-size: 20px;">{{ $job->jobtitle }}</strong></a></h3>
                               
                                <span style="color: black; font-size: 20px;"> {{ $job->company['companyname'] }} </span>
                                
                                <hr style=" border-bottom: 3px dashed #ccc;">
                                </div>
                                
                                


                                
                                
                                
                                
                                @endforeach
                                
                                </div>
                                
                                
                            </section>
                        </div>
                        <div class="3u">
                            <section id="sidebard2">
                                <header>
                                    <h2>Sidebar 2</h2>
                                </header>
                                <ul class="style1">
                                    <li class="first"><span class="fa fa-check"></span><a href="#">Maecenas luctus lectus at sapien</a></li>
                                    <li><span class="fa fa-check"></span><a href="#">Etiam rhoncus volutpat erat</a></li>
                                    <li><span class="fa fa-check"></span><a href="#">Donec dictum metus in sapien</a></li>
                                    <li><span class="fa fa-check"></span><a href="#">Integer gravida nibh quis urna</a></li>
                                    <li><span class="fa fa-check"></span><a href="#">Etiam posuere augue sit amet nisl</a></li>
                                    <li><span class="fa fa-check"></span><a href="#">Mauris vulputate dolor nibh</a></li>
                                    <li><span class="fa fa-check"></span><a href="#">Nulla luctus eleifend purus</a></li>
                                </ul>
                                <a href="#" class="button">Full Article</a>
                            </section>
                        </div>
                    </div>

                </div>  
            </div>

            <!-- Footer -->
            <div id="footer">
                <div class="container">
                    <div class="row">
                        <div class="3u">
                            <section id="box1">
                                <header>
                                    <h2>Nulla facilisi</h2>
                                </header>
                                <ul class="style3">
                                    <li class="first">
                                        <p class="date"><a href="#">10.03.2012</a></p>
                                        <p><a href="#">Vestibulum sem magna, elementum ut, vestibulum facilisis. Nulla facilisi. Cum sociis natoque penatibus.</a></p>
                                    </li>
                                    <li>
                                        <p class="date"><a href="#">10.03.2012</a></p>
                                        <p><a href="#">Pellentesque erat erat, tincidunt in, eleifend, malesuada bibendum. Suspendisse sit amet  in eros bibendum condimentum. </a> </p>
                                    </li>
                                </ul>
                            </section>
                        </div>
                        <div class="6u">
                            <section id="box2">
                                <header>
                                    <h2>Donec dictum metus</h2>
                                </header>
                                <div> <a href="#" class="image full"><img src="images/pics02.jpg" alt=""></a> </div>
                                <p>Nulla enim eros, porttitor eu, tempus id, varius non, nibh. Duis enim nulla, luctus eu, dapibus lacinia, venenatis id, quam. Vestibulum imperdiet, magna nec eleifend rutrum, nunc lectus vestibulum velit, euismod lacinia quam nisl id lorem. Quisque erat. Vestibulum pellentesque, justo mollis pretium suscipit, justo nulla blandit libero, in blandit augue justo quis nisl.</p>
                            </section>
                        </div>
                        <div class="3u">
                            <section id="box3">
                                <header>
                                    <h2>Gravida ipsum</h2>
                                </header>
                                <ul class="style1">
                                    <li class="first"><a href="#">Pellentesque quis elit non lectus eleifend purus condimentum.</a></li>
                                    <li><a href="#">Lorem ipsum dolort, consectetuer adipiscing dictum metus sapien.</a></li>
                                    <li><a href="#">Phasellus nec dictum metus in sapien pellentesque congue.</a></li>
                                    <li><a href="#">Cras vitae metus aliquam risus dictum metus in sapien pharetra.</a></li>
                                    <li><a href="#">Duis non dictum metus in sapien ante in metus commodo euismod lobortis.</a></li>
                                </ul>
                            </section>
                        </div>
                    </div>
                </div>
            </div>

            
            
            
        </div>
    </body>
    <hr style="margin-top: 70px;">
    @include('footer') 
</html>