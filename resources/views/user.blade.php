@extends('layout')

@section('content')
<style type="text/css">
    
    @media screen and (max-width: 767px){
        .userrecords{
            width: 100%;
        }
    
    }
</style>
<body style="background-color: #ECEDF0;">
	<div class="container form-control" style="margin-top: 60px;">
		<h4 style="margin-top: 10px;">Here Is Your Profile</h4>
		<hr>
		<div class="row">
			<div class="col">	
				<img src="/images/preview.png" style="width:150px; height: 150px;" class="form-control">
			</div>

			<div class="col-md-10">
				<div class="">
					<label><strong>Email</strong> <span class="col-md-6">:</span>{{ $user->email }}</label><br>
				</div>

				<div class="">
					<label><strong>Keyword</strong> <span class="col">:</span> {{ $user->title }}</label>
				</div>

				<div class="">
					<label><strong>Phone</strong><span class="col">:</span>{{ $user->phone }}</label>
				</div>

				<div class="">
					<label><strong>CV</strong><span class="col">:</span>{{ $user->cv }}</label>
				</div>

				<div class="">
					<label><strong>Last Updated</strong><span class="col">:</span>{{ $user->updated_at }}</label>
				</div>
			</div>
		</div>

		<br>

		<div class="row">
		<div class = "form-group">
			<a href="{{ route('profile') }}" style="cursor:pointer; margin-left: 16px;" class="btn btn-lg btn-info">Update CV</a>
		</div>
		<div class = "form-group">
			<a href="{{ route('edit') }}" style="cursor:pointer; margin-left: 20px;" class="btn btn-lg btn-info" >Change Password</a>
		</div>
		</div>
	</div>

<div class="container userrecords form-control" style="margin-top: 30px;">
<h4 style="margin-top: 10px;">Previous Applications</h4>
	<hr>

<table class="table">
  <thead class="thead-light">
  	
    <tr>
      <th scope="col">Job Title</th>
      <th scope="col">CompanyName</th>
      <th scope="col">Location</th>
      <th scope="col">Posted At</th>
    </tr>
  </thead>
  <tbody>
  	@if(isset($var))
  	@foreach($var as $va)
    <tr>
      <th scope="row">{{ $va->jobtitle }}</th>
      <td>{{ $va->company['companyname'] }}</td>
      <td>{{ $va->location }}</td>
      <td>{{ $va->created_at }}</td>
    </tr>
    @endforeach 
    @else
    <th colspan="4" style="text-align: center">Nothing to Show...</th>
    @endif   
  </tbody>
</table>

</div>
</body>

@endsection