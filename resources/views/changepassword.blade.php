@extends('layout')

@section('content')

<body style="background-color: #ECEDF0;">
		<form action="{{ route('update', $user->id) }}" method="post" class="container form-control" style="width: 500px; background-color: white; margin-top: 80px;" >
                @csrf
                {{ method_field('PATCH')}}
                
            <div class="card-body p-6">
                  <h4 style=" text-align: center">Reset your account</h4>
                  <hr>
                  <div class="form-group">
                    <label class="form-label">Old Password</label>
                    <input type="password" class="form-control{{ $errors->has('oldpassword') ? ' is-invalid' : '' }}" name="oldpassword" id="oldpassword" aria-describedby="emailHelp" placeholder="Enter Old Password" value="{{ old('oldpassword') }}" >
                     @if ($errors->has('oldpassword'))
                          <span class="invalid-feedback">
                              <strong>{{ "Wrong Old Password" }}</strong>
                          </span>
                      @endif
                  </div>
                  <div class="form-group">
                    <label class="form-label">New Password</label>
                    <input type="password" class="form-control" name="password" id="password" placeholder="New Password">
                  </div>

                  <div class="form-group">
                    <label class="form-label">Confirm Password</label>
                    <input type="password" class="form-control{{ $errors->has('password-confirm') ? ' is-invalid' : '' }}" name="password_confirmation" id="password-confirm" placeholder="Confirm Password">
                    @if ($errors->has('password-confirm'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('password-confirm') }}</strong>
                        </span>
                    @endif
                  </div>
                    <div class="form-footer">
                    <button type="submit" class="btn btn-info btn-block">Reset</button>
                  </div>
            </div>
    	</form>
</body>


@endsection