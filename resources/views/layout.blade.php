<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="footer, address, phone, icons" />
    <link rel="icon" href="../../../../favicon.ico">

    <title>Za</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Exo">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

    
    <link rel="stylesheet" href="/footer/footer.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
    <link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">
    <link href="/css/layout.css" rel="stylesheet">
    <script type="text/javascript" src="/js/app.js" ></script> 

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    

</head>

<header>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <img src="/images/za.png" width="50px" height="50px">&nbsp;
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      
      @if(auth('user')->user())
      <li class="nav-item active">
        <a style="color: #8e000E;" class="btn btn-light" href="{{ route('user') }}">Dashboard</a>
      </li>
       @else
      <li class="nav-item">
        <a style="color: #8e000E;" class="btn btn-light" href="{{ route('home.index') }}">Home</a>
      </li>
      <li class="nav-item">
        <a style="color: #8e000E;" class="btn btn-light" href="{{ route('auth.login') }}">Login</a>
      </li>
      <li class="nav-item">
        <a style="color: #8e000E;" class="btn btn-light" href="{{ route('register') }}">Register</a>
      </li>
      @endif
      <li class="nav-item">
        <a style="color: #8e000E;" class="btn btn-light" href="{{ route('search.index') }}">Search Job</a>
      </li>
    </ul>
    @if(auth('user')->user())

                    <div class="dropdown">

                        <a href="#" class=""
                           data-toggle="dropdown" role="button"
                           aria-haspopup="true" aria-expanded="false">
                            {{ auth('user')->user()->firstname }}
                            <span class="caret">
                                <img class="circ" src="{{ Gravatar::get(auth('user')->user()->email)  }}">
                            </span></a>

                        <ul class="dropdown-menu">
                            <li>

                                <a style="color: blue; text-align: center" class="btn btn-light btn-block" href="{{ route('user') }}">Profile</a>
                                
                              
                                
                                <a style="color: #8e000E;" class="btn btn-light btn-block" href="#" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit()">Logout</a>

                                
                                <form action="{{ route('auth.logout') }}" id="logout-form" method="POST" style="display: none">
                            {{ csrf_field() }}
                        </form>
                            </li>
                        </ul>
                    </div>
                    
                @endif   
    <form class="form-inline my-2 my-lg-0" method="POST" action="/searchjob">
      @csrf
      <input class="form-control mr-sm-2" name="searchjob" type="search" placeholder="Search" aria-label="Search">
      <button style="color: #8e000E;" class="btn btn-light my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>

</header>



@yield('content')


@include('footer')

</html>
