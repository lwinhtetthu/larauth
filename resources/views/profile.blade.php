@extends('layout')

@section('content')
<body style="background-color: #ECEDF0;">
<div class="container" style="width: 800px; margin-top: 40px">
	
<form action="/profile/{{ $user->id }}" method="post" class="form-control" enctype="multipart/form-data">	
	{{ csrf_field() }}
    {{ method_field('patch') }}


<h4 class="header" style=" margin-top: 10px;">Update your profile HERE</h4>
<hr>
  <div class="form-row">    
  	<div class="form-group col-md-6">
        <label for="email"><strong>Email:</strong></label>
                    
        <input type="text" name="email" maxlength="50" id="email" class="form-control" 
        value="{{ $user->email }}" />
               
  	</div>
 	<div class="form-group col-md-6">
        <label for="phone"><strong>Phone Number:</strong></label>
        
        <input name="phone" type="text" maxlength="20" id="phone"  class="form-control" 
        value=" {{ $user->phone }} "  />
    </div>
  </div>
  


  <div class="form-row">  
   	<div class="form-group col-md-6">
        <label for="firstname"><strong>First Name:</strong></label>
                               
        <input name="firstname" type="text" maxlength="50" id="firstname" class="form-control" value="{{ $user->firstname }}" />
  	</div>

  	<div class="form-group col-md-6">
        <label for="lastname"><strong>Last Name:</strong></label>
                               
        <input name="lastname" type="text" maxlength="50" id="lastname" class="form-control" value="{{ $user->lastname }}" />
    </div>
   </div>  


   <div class="form-row">
     <div class="form-group col-md-6">
        <label for="title"><strong>Job Title:</strong></label>
        
        <input name="title" type="text" maxlength="50" id="title" class="form-control" 
        value="{{ $user->title }}" />
   	 </div>


     <div class="form-group col-md-6">
        <label for="jobtype"><strong>Your Primary Job Function</strong></label>
        	<select name="jobtype_id" id="jobtype" class="sign-up-select form-control">        
                    <option value="0">Choose Job Function</option>
                    @foreach($jobtypes as $jobtype)
                    <option value="{{ $jobtype->id }}"
                    @if($user->jobtype_id == $jobtype->id)
                     selected
                    @endif 
                    >
                    {{ $jobtype->jobtypes }}</option>
                    @endforeach;
                    
                    
        	</select>
        
    	</div>
	</div>

    <div class="wrap-input form-group">
        <label for="level"><strong>Your Experience Level</strong></label>
        <select name="level_id" id="level" class="sign-up-select form-control">
                <option value="0">Choose Experience Level</option>
                @foreach($levels as $level)
                    <option value="{{ $level->id }}" 
                    @if($user->level_id == $level->id) selected 
                    @endif>{{ $level->levels }}</option>
                @endforeach

        </select>
    </div>


    <div class="wrap-input form-group">
        <label for="location"><strong>Your Current Location</strong></label>
        <select name="location_id" id="location" class="sign-up-select form-control">
                <option value="0">Choose Location</option>
                @foreach($locations as $location)
                    <option value="{{ $location->id }}"
                    @if($user->location_id == $location->id) selected 
                    @endif>{{ $location->locations }}</option>
                @endforeach

        </select>
                                
    </div>

    <div class="wrap-input form-group div-sign-up-upload-cv form-control">
        <div>
            <label><strong>Want To Update Your CV. Do It Here</strong></label>
            
        </div>
        
        <div style="display: inline-block; width: 100%;">        
            <input type="file" name="cv" id="cv" />
            <span style="color: green;" class="container">( {{ $user->cv }} )</span> <strong>Your CV</strong> 
        </div>
    </div>

	<br>
	<div class="form-group">
            <button style="cursor:pointer" type="submit" class="btn btn-lg btn-info btn-block" >Update</button>
       </div>
</form>
</div>
</body>
@endsection