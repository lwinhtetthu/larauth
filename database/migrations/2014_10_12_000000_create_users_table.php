<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type')->default('default');
            $table->string('email');
            $table->string('password');
            $table->string('firstname');
            $table->string('lastname');
            $table->integer('phone');
            $table->string('title');
            $table->unsignedInteger('jobtype_id');
            $table->unsignedInteger('level_id');
            $table->unsignedInteger('location_id');
            $table->string('cv');
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('jobtype_id')->references('id')->on('jobtypes');
            $table->foreign('level_id')->references('id')->on('levels');
            $table->foreign('location_id')->references('id')->on('locations');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
