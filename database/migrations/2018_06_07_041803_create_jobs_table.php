<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('jobtitle');
            $table->unsignedInteger('companyname');
            $table->string('salary');
            $table->string('location');
            $table->unsignedInteger('jobtime_id');
            $table->unsignedInteger('jobtype');
            $table->string('description');
            $table->string('requirement');
            $table->string('aboutcompany');
            $table->timestamps();

            $table->foreign('companyname')->references('id')->on('companies');
            $table->foreign('jobtype')->references('id')->on('jobtypes');
            $table->foreign('jobtime_id')->references('id')->on('jobtimes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
